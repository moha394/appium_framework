package test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;

public class Test_Base {
	
	static AndroidDriver<WebElement> driver;
	
	
	
	
	
	@BeforeSuite
	public static void openApp() throws MalformedURLException 
	{
		DesiredCapabilities caps = new DesiredCapabilities();		
		File app = new File(System.getProperty("user.dir")+"\\app\\testApp.apk");
		caps.setCapability("platformName", "Android");
		caps.setCapability("platformVersion", "6.0");
		caps.setCapability("deviceName", "Nexus6P");
		caps.setCapability("app", app.getAbsolutePath());
		caps.setCapability("automationName", "UiAutomator1");
		driver = new AndroidDriver<WebElement>(new URL("http://localhost:4723/wd/hub"), caps);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	
	}
	
	@AfterSuite
	public static void close() throws InterruptedException {
		
		Thread.sleep(3000);
		driver.quit();
		}

}
