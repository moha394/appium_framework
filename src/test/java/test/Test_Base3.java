package test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;

public class Test_Base3 {
	
	static AppiumDriver<MobileElement> driver;
	@Test
	public static void openApp() throws MalformedURLException 
	{
		DesiredCapabilities caps = new DesiredCapabilities();
		
		File app = new File(System.getProperty("user.dir")+"\\app\\selendroid.apk");
		caps.setCapability("platformName", "Android");
		caps.setCapability("platformVersion", "6.0");
		caps.setCapability("deviceName", "Nexus6P");
		caps.setCapability("app", app.getAbsolutePath());
		caps.setCapability("automationName", "UiAutomator1");
		
		
		
		
		//This Capabilities for Android Settings only
//		caps.setCapability("appPackage", "com.android.settings");
//		caps.setCapability("appActivity", "com.android.settings.Settings");

		
		 driver = new AndroidDriver<MobileElement>(new URL("http://localhost:4723/wd/hub"), caps);
		 driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		//driver.findElement(by, using)
		//driver.findElement(By.id("my_text_fieldCD")).sendKeys("Hello");
		WebElement btn = driver.findElement(By.id("io.selendroid.testapp:id/visibleButtonTest"));
		
		
//		
//		// long press implementing **To speciffy the time we use press + wait + release functions
//		new TouchAction<>(driver).longPress(LongPressOptions.longPressOptions().withElement(ElementOption.element(btn))).perform();
//		driver.findElement(By.id("android:id/button1")).click();
//		
		
//		
		
		
		
		// click on element with only coordinates
		System.out.println(btn.getLocation().getX());
		System.out.println(btn.getLocation().getY());	
		System.out.println(btn.getSize().getWidth());
		System.out.println(btn.getSize().getHeight());	
		int xCenter = btn.getLocation().getX()+(btn.getSize().getWidth())/2;
		int yCenter = btn.getLocation().getY()+(btn.getSize().getHeight())/2;	
		System.out.println(xCenter);
		System.out.println(yCenter);	
		//new TouchAction<>(driver).tap(PointOption.point(xCenter, yCenter)).perform();
//		
//		

		
//		List<WebElement> li = driver.findElements(By.className("android.widget.TextView"));
//		for (WebElement webElement : li) {
//			System.out.println(webElement.getAttribute("text"));
//		}
		
		
		//Using UISelector
		
		
		
		//UI Selector By ID
			//driver.findElementByAndroidUIAutomator("UiSelector().resourceId(\"io.selendroid.testapp:id/my_text_field\")").sendKeys("Hello");
		
		//UI Selector By ClassName
			//		 List<WebElement> li = driver.findElementsByAndroidUIAutomator("UiSelector().className(\"android.widget.TextView\")");
			//			for (WebElement webElement : li) {
			//				System.out.println(webElement.getAttribute("text"));
			//			}
		 
//		 WebElement container1  =    driver.findElementsByAndroidUIAutomator("UiSelector().resourceId(\"com.android.settings:id/category\")").get(0);
//			
//			List<WebElement> li1 = container1.findElements(By.id("com.android.settings:id/title"));
//			for (WebElement webElement : li1) {
//				System.out.println(webElement.getAttribute("text"));
//			}
//		 //working with Android Settings
//		 WebElement container  =    driver.findElementsByAndroidUIAutomator("UiSelector().resourceId(\"com.android.settings:id/category\")").get(1);
//		
//		List<WebElement> li = container.findElements(By.id("com.android.settings:id/title"));
//		for (WebElement webElement : li) {
//			System.out.println(webElement.getAttribute("text"));
//		}
		//String x = driver.findElement(By.id("visibleButtonTestCD")).getAttribute("text");
		//System.out.println(x);
	}
	
	@AfterMethod
	public static void close() {
	//	driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

		driver.quit();
	}

}
