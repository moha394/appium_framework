package test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.ScrollAction;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.LongPressOptions;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.offset.ElementOption;
import io.appium.java_client.touch.offset.PointOption;

public class Test_Base2 {
	static AppiumDriver<MobileElement> driver;
	@Test
	public static void openApp() throws MalformedURLException 
	{
		DesiredCapabilities caps = new DesiredCapabilities();
		
		//File app = new File(System.getProperty("user.dir")+"\\app\\selendroid.apk");
		caps.setCapability("platformName", "Android");
		caps.setCapability("platformVersion", "6.0");
		caps.setCapability("deviceName", "Nexus6P");
		//caps.setCapability("app", app.getAbsolutePath());
		caps.setCapability("automationName", "UiAutomator1");
		
		
		
		
		//This Capabilities for Android Settings only
		caps.setCapability("appPackage", "com.android.settings");
		caps.setCapability("appActivity", "com.android.settings.Settings");

		
		// driver = new AndroidDriver<MobileElement>(new URL("http://localhost:4723/wd/hub"), caps);
		driver.get("https://www.google.com");
		 driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		 
		 
		 
		 
		 
	//WebElement btn = driver.findElement(By.id("io.selendroid.testapp:id/visibleButtonTest"));
	
		 
		 
		 /// scrolling element 
		 
//		 driver.findElementByAndroidUIAutomator("new UiScrollable("
//		        + "new UiSelector().scrollable(true)).scrollIntoView("                      
//		        + "new UiSelector().textContains(\"Printing\"));").click();
//			//	new TouchAction<>(driver).moveTo(ElementOption.element(btn)).perform();

		 
//		 driver.findElementByAndroidUIAutomator("new UiScrollable("
//		        + "new UiSelector().scrollable(true)).scrollIntoView().scrollToEnd(20"                      
//		        + ");");
			//	new TouchAction<>(driver).moveTo(ElementOption.element(btn)).perform();

			
 
		 
	}
	
	@AfterMethod
	public static void close() {
	//	driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

		driver.quit();
	}

}
