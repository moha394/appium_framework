package test;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import pages.Edit_Profile_Page;
import pages.Home_Page;
import pages.Profile_Page;

public class ChangeProfile_Test extends Test_Base {

	Home_Page HomePageObject;
	Profile_Page ProfilePageOBject;
	Edit_Profile_Page EditProfileObject;
	
	
	@Test 	
	public void change_name() throws InterruptedException {
		//Open Home Page and go to ProfilePage
		HomePageObject = new Home_Page(driver);
		HomePageObject.OpenProfile_Page();
		
		
		//From Profile Page go to Edit Profile User
		ProfilePageOBject = new Profile_Page(driver);
		ProfilePageOBject.OpenEditPage();
		
		//Change Name
		EditProfileObject = new Edit_Profile_Page(driver);
		EditProfileObject.Change("Name","Ahmed");
		EditProfileObject.Change("Email","moha3942014@gmail.com");
		EditProfileObject.Change("Phone Number","0103455670");
		EditProfileObject.Change("Address","46 staku aammajj");
	}
	
	
	
	
	
}
