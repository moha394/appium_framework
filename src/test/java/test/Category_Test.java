package test;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import pages.Category_Page;
import pages.Edit_Profile_Page;
import pages.Home_Page;
import pages.Profile_Page;

public class Category_Test extends Test_Base {

	Home_Page HomePageObject;
	Category_Page CategoryPageObject;
	
	@Test 	
	public void change_name() throws InterruptedException {
		//Open Home Page and go to CatgeoryPage
		HomePageObject = new Home_Page(driver);
		HomePageObject.OpenCategory_page();
		
		Thread.sleep(1000);
		
		//Open Electronics Page
		CategoryPageObject = new Category_Page(driver);
		CategoryPageObject.Open_Section("Electronics & Gadgets");
		driver.pressKey(new KeyEvent(AndroidKey.BACK));
		CategoryPageObject.Open_Section("Fashions");
		driver.pressKey(new KeyEvent(AndroidKey.BACK));		
		
	}
	
	
	
	
	
}
