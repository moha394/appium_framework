package testWeb;

import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

import pagesWeb.Home_Page;
import pagesWeb.Login_Page;
import pagesWeb.User_Register;

public class LoginTest extends Test_BaseForBrowser{

	Home_Page HomePageObject; 
	User_Register UserRegisterObject;
	Login_Page LoginObject;

	@Test 
	void OpenBrowser() throws InterruptedException {
		

		HomePageObject = new Home_Page(driver);
		//HomePageObject.OpenRegistrationPage();
		
		
		//UserRegisterObject = new User_Register(driver);
		//UserRegisterObject.RegistrationFill("Mohamed", "Ahmed", "moha111394222222233d014@gmail.com", "Bm1234567890", "Bm1234567890");
		
		//HomePageObject.UserLogout();
		HomePageObject.OpenLoginPage();
		
		
		LoginObject = new Login_Page(driver);
		LoginObject.loginUser("moha111394222222233d014@gmail.com", "Bm1234567890");
		LoginObject.checkRememberMe();
		LoginObject.PressLoginButton();
	}


}
