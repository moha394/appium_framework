package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.ElementOption;

public class Base_Page {
	
	
 protected AndroidDriver<WebElement> driver;

public Base_Page(AndroidDriver<WebElement>driver)
{
	PageFactory.initElements(driver, this);
}
	
public WebElement getelement(List<WebElement>li , String text) {	
	WebElement el = null;
	
	for (WebElement webElement1 : li) {

		if(webElement1.getAttribute("text").contains(text))
			{el = webElement1;break;}
	
	}
	
	return el;
}

	
}
