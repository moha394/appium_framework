package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.appium.java_client.android.AndroidDriver;

public class Profile_Page extends Base_Page {

	public Profile_Page(AndroidDriver<WebElement> driver) {
		super(driver);
	}


	@FindBy(id = "com.solodroid.solomerce:id/btn_edit_user")
	WebElement editbtn;

	@FindBy(id = "com.solodroid.solomerce:id/btn_order_history")
	WebElement orderhistorybtn;


	@FindBy(id = "com.solodroid.solomerce:id/btn_rate")
	WebElement ratebtn;


	@FindBy(id = "com.solodroid.solomerce:id/btn_share")
	WebElement sharebtn;
	
	
	public void OpenEditPage() {editbtn.click();}
	public void OpenOrderPage() {orderhistorybtn.click();}
	
	
	
	
	
}
