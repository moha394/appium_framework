package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.appium.java_client.android.AndroidDriver;

public class Category_Page extends Base_Page {

	public Category_Page(AndroidDriver<WebElement> driver) {
		super(driver);
	}

	
	@FindBy(id="Navigate up")
	WebElement backbtn;
	
	
	@FindBy(className = "android.widget.TextView")
	List<WebElement> li;
	
	
	public void Open_Section(String section) {
			getelement(li, section).click();
	}
	public void getback() {backbtn.click();}
	
	
	
	
}
