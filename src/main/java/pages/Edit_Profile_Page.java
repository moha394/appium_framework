package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class Edit_Profile_Page extends Base_Page {

	public Edit_Profile_Page(AndroidDriver<WebElement> driver) {
		super(driver);
	}

	@FindBy(id = "android:id/edit")
	WebElement edit_Name;
	
	@FindBy(id = "android:id/button1")
	WebElement click_ok;
	
	@FindBy(className = "android.widget.TextView")
	List<WebElement> li;
	
	
	
	public void Change(String field ,String name) {
	
		getelement(li,field).click();
		edit_Name.clear();
		edit_Name.sendKeys(name);
		click_ok.click();
	}
	
	
	
		
		

		
	

}
