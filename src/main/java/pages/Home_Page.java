package pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

public class Home_Page extends Base_Page {

	public Home_Page(AndroidDriver<WebElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	
	
	@FindBy(id = "Profile")
	WebElement profilebtn;
	
	@FindBy(id = "Help")
	WebElement helpbtn;
	
	@FindBy(id = "Category")
	WebElement Categorybtn;
	
	@FindBy (id = "Recent")
	WebElement Recentbtn;
	
	
	public void OpenProfile_Page() {profilebtn.click();}
	public void Openhelp_Page() {helpbtn.click();}
	public void OpenCategory_page() {Categorybtn.click();}

	
	
	
	
	
	
	
	
	
	
}
