package pagesWeb;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

public class Home_Page extends Base_Page {

	public Home_Page(AndroidDriver<WebElement> driver) {
		super(driver);
		
	}

	
	@FindBy(className  = "ico-register")
	WebElement RegisterLink;
	
	@FindBy(className  = "ico-login")
	WebElement LoginLink;
	
	@FindBy(className  = "ico-logout")
	WebElement LogoutLink;
	
	public void OpenRegistrationPage() {
	
		clickButton(RegisterLink);	
	}
	
	public void OpenLoginPage() {
		
		clickButton(LoginLink);
		
	}
	
	public void UserLogout() {
		clickButton(LogoutLink);
	}
}
