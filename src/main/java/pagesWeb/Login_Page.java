package pagesWeb;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.appium.java_client.android.AndroidDriver;

public class Login_Page extends Base_Page {

	public Login_Page(AndroidDriver<WebElement> driver) {
		super(driver);
	}
	@FindBy(id = "Email")
	WebElement email;
	
	@FindBy(id = "Password")
	WebElement password;
	
	@FindBy (id = "RememberMe")
	WebElement RememberMe;

	@FindBy(xpath = "//input[@class='button-1 login-button']")
	WebElement LoginButton;
	
	@FindBy(linkText = "My account")
	WebElement result;

	public void loginUser(String emailTxt , String passwordTxt ) {
		
		
		SetTextElement(email, emailTxt);
		SetTextElement(password, passwordTxt);
	}
	
	public void checkRememberMe() {
		if(!RememberMe.isSelected()) 
			RememberMe.click();
	}

	public void PressLoginButton() {
		
		clickButton(LoginButton);
	
	}

	public void ReturnResult(String resultTxt)
	{
		
		AssertElement(result, resultTxt);
	}
}
