package pagesWeb;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.offset.ElementOption;

public class User_Register extends Base_Page{

	public User_Register(AndroidDriver<WebElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	
	
	@FindBy(id = "gender-male")
	WebElement Gender_Male;
	
	
	@FindBy(id  = "FirstName")
	WebElement FirstName;
	
	
	@FindBy(id = "LastName")
	WebElement LastName;

	
	@FindBy(id = "Email")
	WebElement Email;
	
	
	@FindBy(id = "Password")
	WebElement Password;
	
	@FindBy(id = "ConfirmPassword")
	WebElement ConfirmPassword;
	
	@FindBy(id = "register-button")
	WebElement register_button;
	
	@FindBy(className = "result")
	WebElement result;
	
	
	public void RegistrationFill(String FirstNameTxt,String LastNameTxt,String EmailTxt,String PasswordTxt,String ConfirmPasswordTxt) throws InterruptedException {


		clickButton(Gender_Male);
		SetTextElement(FirstName, FirstNameTxt);
		SetTextElement(LastName, LastNameTxt);
		SetTextElement(Email, EmailTxt);
		SetTextElement(Password, PasswordTxt);
		SetTextElement(ConfirmPassword, ConfirmPasswordTxt);
		clickButton(register_button);
		
		
	
	}
	public void ReturnResult(String resultTxt)
	{
		
		AssertElement(result, resultTxt);
	}
}
